package au.edu.uts.aip.sht.Room;

import java.io.*;
import java.sql.*;
import javax.enterprise.context.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.*;
import javax.naming.*;

@Named
@RequestScoped
public class RoomController implements Serializable {
	
	private int id;
    private String username;
	
	// Returns RoomDTO (row) based on Room's ID (primary key).
	public RoomDTO getRoomByID(int id) throws NamingException, SQLException {
		RoomDAO dao = new RoomDAO();
		return dao.getRoomByID(id);
	}
	
	
	// Makes a booking based on GET parameter for ID and logged in user.
	public String makeBooking(String username) throws NamingException, SQLException {
		
		// Initialises a new UserDAO to create the new user.
		RoomDAO dao = new RoomDAO();
		
		// Sources row parameter as opposed to input field for Room ID due to buttons
		// for each row.
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		int passedInt = Integer.parseInt(ec.getRequestParameterMap().get("id"));
		
		// Make booking.
		dao.makeBooking(passedInt, username);
		
		// Refresh page.
		return "booking?faces-redirect=true";
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}