package au.edu.uts.aip.sht.Room;

import java.io.*;
import javax.enterprise.context.*;
import javax.inject.Named;

@Named
@ApplicationScoped
public class Room implements Serializable {
	
	private int id;
    private String username;
	private String fullname;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

}