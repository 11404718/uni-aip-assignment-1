package au.edu.uts.aip.sht.Room;

import java.sql.*;
import java.util.*;
import javax.naming.*;

public interface RoomDAOInterface {
	
	// Methods for the DAO are declared in an interface for
	// future-proofing in the event the system is moved
	// to a different database. Please see RoomDAO for 
	// subroutine explanations.
	
	public RoomDTO getRoomByID(int id) throws NamingException, SQLException;
	
	public ArrayList<RoomDTO> findAll()  throws NamingException, SQLException;
	
	public void makeBooking(int id, String username) throws NamingException, SQLException ;
	
}
