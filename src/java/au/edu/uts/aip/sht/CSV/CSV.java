package au.edu.uts.aip.sht.CSV;

import au.edu.uts.aip.sht.Room.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.naming.*;

public class CSV {
	
	public static String returnCSV() throws NamingException, SQLException {
		
		// Initialisations and declarations of constants.
		final String COMMA = ",";
		final String NEWLINE = "\n";
		StringWriter buildString = new StringWriter();
		
		
		// Generate DAO and retrieve room list.
		RoomDAO dao = new RoomDAO();
		ArrayList<RoomDTO> bDTO = new ArrayList<RoomDTO>();
		bDTO = dao.findAll();
		
		
		// Append each row to the StringWriter.
		for (RoomDTO item : bDTO) {
			buildString.append(String.valueOf(item.getId()));
			buildString.append(COMMA);
			buildString.append(String.valueOf(item.getFullname()));
			buildString.append(COMMA);
			buildString.append(NEWLINE);
		}
		
		
		// Return build CSV.
		return buildString.toString();
		
	}

}
