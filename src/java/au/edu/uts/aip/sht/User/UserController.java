package au.edu.uts.aip.sht.User;

import java.io.*;
import java.security.*;
import java.sql.*;
import javax.faces.application.*;
import javax.enterprise.context.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.validation.constraints.*;

@Named
@SessionScoped
public class UserController implements Serializable {

	private String username;
	private String password;
	private String fullname;
	
	// Allow the user to log in via container based security.
	// Reference is Week5 lab.
	public String login() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		
		try {
			request.login(username, password);
		} catch (ServletException e) {
			context.addMessage(null, new FacesMessage("The supplied credentials were incorrect. Please try again."));
			logout();
			return null;
		}
		
		return "/system/home?faces-redirect=true";
		
	}

	
	// Allow the user to log out of container based security.
	// Reference is Week5 lab.
	public String logout() {

		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		try {
			request.logout();
		} catch (ServletException e) {
			context.addMessage(null, new FacesMessage(e.getMessage()));
		}
		
		return "/login?faces-redirect=true";
		
	}
	
	public String register() throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialises a new UserDAO to create the new user.
		UserDAO dao = new UserDAO();
		dao.register(username, password, fullname);
		
		
		// Destroys the UserConroller and returns to the homepage.
		return logout();
		
	}
	
	public String changePassword() throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialises a new UserDAO to create the new user.
		UserDAO dao = new UserDAO();
		dao.changePassword(username, password);
		
		
		// Redirect to account management page.
		return "/system/account?faces-redirect=true";
		
	}
	
	public String deleteAccount() throws NamingException, SQLException {
		
		// Initialises a new UserDAO to create the new user.
		UserDAO dao = new UserDAO();
		dao.deleteAccount(username);
		
		
		// Destroys the UserConroller and returns to the homepage.
		return logout();
		
	}
	
	@Size(min=3)
	public String getUsername() {
	  return username;
	}

	public void setUsername(String username) {
	  this.username = username;
	}
	
	@Size(min=8)
	public String getPassword() {
	  return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
}