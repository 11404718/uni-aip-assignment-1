package au.edu.uts.aip.sht.User;

import static au.edu.uts.aip.sht.Hash.Sha.hash256;
import java.security.*;
import java.sql.*;
import javax.sql.*;
import java.util.*;
import javax.naming.*;

public class UserDAO implements UserDAOInterface {
	
	// Find user from database based on the user's name.
	public UserDTO find(String username) throws NamingException, SQLException {

		// Initialise variables and declarations.
		UserDTO result = new UserDTO();
		
		
		// Initialise the datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username.
			PreparedStatement stmt = conn.prepareStatement("SELECT USERNAME, PASSWORD, FULLNAME FROM USER WHERE USERNAME = ?");
			stmt.setString(1, username);

			
			// Execute query and return result set.
			ResultSet rs = stmt.executeQuery();


			// "if next" is used over "while next" as this
			// is only for one row.
			if (rs.next()) {
				
				// Add retrieved data to the DTO.
				result.setUsername(rs.getString("USERNAME"));
				result.setPassword(rs.getString("PASSWORD"));
				result.setFullname(rs.getString("FULLNAME"));
				
			}
			
		}
		
		// Return the DTO.
		return result;
	
	}
	
	
	// Return all users in the database.
	public ArrayList<UserDTO> findAll() throws NamingException, SQLException {
		
		// Initialise variables and declarations.
		ArrayList<UserDTO> result = new ArrayList<>();
		
		
		// Initilise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Create statement. No PreparedStatement is required due
			// to lack of parameters.
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT USERNAME, PASSWORD, FULLNAME FROM FINDALLUSERS");

			
			// Iterate through returned rows in resultset.
			while (rs.next()) {
				
				// Temp DTO declared to be added to
				// the arraylist.
				UserDTO user = new UserDTO();
				user.setUsername(rs.getString("USERNAME"));
				user.setPassword(rs.getString("PASSWORD"));
				user.setFullname(rs.getString("FULLNAME"));
				
				// Add temp DTO to arraylist.
				result.add(user);
			}
		}
		
		// Return the DTO arraylist.
		return result;
		
	}
	
	
	// Allow a new user to register for the system.
	public void register(String username, String password, String fullname) throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("INSERT INTO \"USER\" VALUES (?, ?, ?, FALSE)");
			pstmt.setString(1, username);
			pstmt.setString(2, hash256(password));
			pstmt.setString(3, fullname);

			
			// Execute query.
			pstmt.execute();
		
		}
		
	}
	
	
	// Allow a user to change their password.
	public void changePassword(String username, String password) throws NamingException, SQLException, NoSuchAlgorithmException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
			// Prepare statement and intercept username, hashed password and fullname.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET PASSWORD = ? WHERE USERNAME = ?");
			pstmt.setString(1, hash256(password));
			pstmt.setString(2, username);

			// Execute query.
			pstmt.execute();
			
		}
		
	}
	
	
	// Allow a user to delete their own account.
	public String deleteAccount(String username) throws NamingException, SQLException {
		
		// Initialise datasource.
		DataSource ds = (DataSource)InitialContext.doLookup("jdbc/aip");
		
		// Try-with-resources to ensure connections are
		// closed automatically.
		try (Connection conn = ds.getConnection()) {
			
		
			// Prepare statement and intercept username.
			PreparedStatement pstmt = conn.prepareStatement("UPDATE \"USER\" SET DELETED = TRUE WHERE USERNAME = ?");
			pstmt.setString(1, username);

			// Execute query.
			pstmt.execute();
			
		}
		
		// Return to account maintenance page.
		return "account";
		
	}
	
}