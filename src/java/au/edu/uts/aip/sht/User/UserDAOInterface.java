package au.edu.uts.aip.sht.User;

import java.security.*;
import java.sql.*;
import java.util.ArrayList;
import javax.naming.*;

public interface UserDAOInterface {

	// Methods for the DAO are declared in an interface for
	// future-proofing in the event the system is moved
	// to a different database. Please see UserDAO for 
	// subroutine explanations.
	
	public UserDTO find(String username) throws NamingException, SQLException;

	public ArrayList<UserDTO> findAll() throws NamingException, SQLException;
	
	public void register(String username, String password, String fullname) throws NamingException, SQLException, NoSuchAlgorithmException;
	
	public void changePassword(String username, String password) throws NamingException, SQLException, NoSuchAlgorithmException;
	
	public String deleteAccount(String username) throws NamingException, SQLException;
	
}